# Anycast Services

This repo exists primarily to teach the fundamentals of
anycast with as little fluff and magic as possible.

It leverages `cldemo-vagrant`, a project from Cumulus
Networks that sets up a network topology with servers
attached, and `cldemo-config-routing`, which configures
IP routing on the network topology.

Note that the IP routing configuration is done in the
`deploy-bgp-unnumbered.yml` playbook.  This playbook
doesn't do much -- it configures eBGP unnumbered within
the switch fabric, and it also configures eBGP toward
`server01` and `server03`.  These servers are our
anycast hosts, whereas `server02` will be our client
that communicates with the anycast hosts.

The details of the `deploy-bgp-unnumbered.yml` playbook
aren't important, but they are extremely simple and
straightforward if you want to explore them.

Once IP routing is configured, we use a separate playbook
to configure anycast.  This playbook targets only
`server01` and `server03`, and it simply configures
an IP address on a loopback interface and then installs
and configures a basic ExaBGP deployment.

Note that in this simple example, the ExaBGP configuration
uses static routes.  While this is fine for the purposes
of example, this isn't a path that should be followed in
production since it doesn't have the ability to withdraw
itself from the fabric in the event that the service goes
bad.

In this example, there isn't an actual service running.
We simply use `ping` to determine if we can talk to
the anycast service.

## Setting Up

To run this demo, start by running `./vagrant.sh`.  This
will start all of the required VMs.  Starting anything more
or less, or out of order, may have unintended side effects.

Once the topology has been brought up, access the OOB
management server with the `vagrant ssh oob-mgmt-server`.
This is where you will do everything else from this point
forward.

Once you're on the OOB server, you should provision the
IP fabric:

```
cd /ansible
ansible-playbook deploy-bgp-unnumbered.yml
```

Let the playbook run.  You can test connectivity by logging
into `server01` and pinging the other hosts.  Simply
`ssh server01` from the OOB server, and then once you're on
`server01`, just `ping -c3 172.16.2.101` and
`ping -c3 172.16.3.101`.

Once you're satisfied that IP connectivity works, you
can deploy the anycast "service":

```
cd /ansible
ansible-playbook deploy-anycast.yml
```

This playbook is also straightforward.  Some of its
variables are in `/ansible/hosts`, but there are none
in `host_vars` or `group_vars` for the sake of
simplicity.

Once the playbook is finished, you should open three
terminal windows and use `vagrant ssh oob-mgmt-server`
in each of them to log into the OOB server.

> `screen` or `tmux` with one terminal session are
> also options.

Once you have three sessions open to the OOB server,
log into each of the non-OOB servers by SSHing
to them (`server01`, `server02`, `server03`).

On `server01` and `server03`, run `tcpdump` to capture
traffic to this host:

```
sudo tcpdump -enniany icmp
```

This will be how we determine to which host an
anycast request is going.

Now, from `server02`, start a continuous ping:

```
ping 10.0.0.100
```

Let it run and watch your sessions on `server01` and
`server03`.  On whichever one you see data flowing,
cancel your `tcpdump` with `ctrl+c` (`^c`).  Stop
the ExaBGP service with
`sudo systemctl stop exabgp`.  Observe that no
packets are dropped and that there is now traffic
arriving on the other server.

Now poke around and play as much as you'd like!

# THE END
